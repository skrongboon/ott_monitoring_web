var SERVER_CONFIG = {
	"harmonic":[
		{"ip":"192.168.100.1","name":"har1","title":"har 1"}
		,{"ip":"192.168.100.2","name":"har2","title":"har 2"}
		,{"ip":"192.168.100.3","name":"har3","title":"har 3"}
		,{"ip":"192.168.100.4","name":"har4","title":"har 4"}
		,{"ip":"192.168.100.5","name":"har5","title":"har 5"}
		,{"ip":"192.168.100.6","name":"har6","title":"har 6"}
		,{"ip":"192.168.100.7","name":"har7","title":"har 7"}
		,{"ip":"192.168.100.8","name":"har8","title":"har 8"}
		,{"ip":"192.168.100.9","name":"har9","title":"har 9"}
		,{"ip":"192.168.100.10","name":"har10","title":"har 10"}]
	,"elemental":[
		,{"ip":"192.168.100.17","name":"ele17","title":"ele 17","cardN":4}
		,{"ip":"192.168.100.18","name":"ele18","title":"ele 18","cardN":4}
		,{"ip":"192.168.100.19","name":"ele19","title":"ele 19","cardN":4}
		,{"ip":"192.168.100.20","name":"ele20","title":"ele 20","cardN":4}
		,{"ip":"192.168.100.21","name":"ele21","title":"ele 21","cardN":4}
		,{"ip":"192.168.100.22","name":"ele22","title":"ele 22","cardN":4}
		,{"ip":"192.168.100.23","name":"ele23","title":"ele 23","cardN":4}
		,{"ip":"192.168.100.24","name":"ele24","title":"ele 24","cardN":4}
		,{"ip":"192.168.100.25","name":"ele25","title":"ele 25","cardN":4}
		,{"ip":"192.168.100.26","name":"ele26","title":"ele 26","cardN":4}
		,{"ip":"192.168.100.27","name":"ele27","title":"ele 27","cardN":4}
		,{"ip":"192.168.100.28","name":"ele28","title":"ele 28","cardN":4}
		,{"ip":"192.168.100.29","name":"ele29","title":"ele 29","cardN":4}
		,{"ip":"192.168.100.30","name":"ele30","title":"ele 30","cardN":4}
		,{"ip":"192.168.100.31","name":"ele31","title":"ele 31","cardN":4}
		,{"ip":"192.168.100.32","name":"ele32","title":"ele 32","cardN":8}
		,{"ip":"192.168.100.33","name":"ele33","title":"ele 33","cardN":8}
		,{"ip":"192.168.100.34","name":"ele34","title":"ele 34","cardN":8}]
	,"mediaexcel":[
		,{"ip":"192.168.100.35","name":"mediaexcel35","title":"media excel 35"}
		,{"ip":"192.168.100.36","name":"mediaexcel36","title":"media excel 36"}
		,{"ip":"192.168.100.37","name":"mediaexcel37","title":"media excel 37"}
		,{"ip":"192.168.100.38","name":"mediaexcel38","title":"media excel 38"}]
}
var LAYOUT = {
	"harmonic":{
		"root":".block-harmonic"
	}
	,"elemental":{
		"root":".block-elemental"
	}
	,"mediaexcel":{
		"root":".block-mediaexcel"
	}
}
var class_harmonic = {
	dom:{}
	,data:""
	,screen_cpu:""
	,screen_mem:""
	,screen_disk:""
	,screen_power1:""
	,screen_power2:""
	,screen_temp:""
	,screen_alarm:""
	,setDom:function(dom){
		console.log("class_harmonic");
		this.dom = dom
		this.screen_cpu 	= this.dom.querySelector('.cpu .value');
		this.screen_mem 	= this.dom.querySelector('.mem .value');
		this.screen_disk 	= this.dom.querySelector('.disk .value');
		this.screen_power1 	= this.dom.querySelector('.power1 .value');
		this.screen_power2 	= this.dom.querySelector('.power2 .value');
		this.screen_temp 	= this.dom.querySelector('.temp .value');
		this.screen_alarm 	= this.dom.querySelector('.alarm .value');
	}
	,setData:function(data){
		this.data 	= data;
		if(data.data =='request fail'){
			this.dataSys = null;
			return false;
		}else{
			this.dataSys= JSON.parse(data.data);
			return true;
		}
	}
	,updateData:function(){
		this._resetAlarm();
		if(this.dataSys == null){
			this._showAlarm();
			return false;
		}
		this._resetAlarm();
		if(this.dataSys == null){
			this._showAlarm();
			return false;
		}
		this.screen_cpu.innerHTML 	
			= this.dataSys.cpuUtilPercent.resultString+"%";	
		this.screen_mem.innerHTML 	
			= (parseInt(this.dataSys.systemAvaiMem.resultString)/1000000).toFixed(2)+"gb";	
		this.screen_disk.innerHTML 	
			= this.dataSys.diskUtilPercent.resultString+"%";	
		this.screen_power1.innerHTML 	
			= this.dataSys.powerValue1.resultString+"v";
		this.screen_power2.innerHTML 	
			= this.dataSys.powerValue2.resultString+"v";	
		this.screen_temp.innerHTML 	
			= this.dataSys.tempValue.resultString+"C";

		this._setDiskStatus(this.screen_disk, parseInt(this.dataSys.diskUtilPercent.resultString))
		this._setTempStatus(this.screen_temp, parseInt(this.dataSys.tempValue.resultString))
		this._setCpuPctStatus(this.screen_cpu, parseInt(this.dataSys.cpuUtilPercent.resultString))
		this._setPowerStatus(this.screen_power1, parseInt(this.dataSys.powerValue1.resultString))	
		this._setPowerStatus(this.screen_power2, parseInt(this.dataSys.powerValue2.resultString))	
	}
	,_showAlarm:function(){
		this.dom.className += " active-alarm";
	}
	,_resetAlarm:function(){
		this.dom.className = 'server'
	}
	,_setDiskStatus:function(dom,data){
		if(data<80){
			dom.style.color = "#27B04E";
		}else{
			dom.style.color = "red";
		}
	}
	,_setTempStatus:function(dom,data){
		if(data<60){
			dom.style.color = "#27B04E";
		}else{
			dom.style.color = "red";
		}
	}
	,_setCpuPctStatus:function(dom,data){
		dom.classList.remove("active-alarm");
		if(data<80){
			dom.style.color = "#27B04E";
		}else{
			dom.style.color = "red";
			dom.classList.add("active-alarm");
		}
	}
	,_setPowerStatus:function(dom,data){
		dom.classList.remove("active-alarm")
		if(data>220){
			dom.style.color = "#27B04E";
		}else{
			dom.style.color = "orange";
		}
		if(isNaN(data) || data<0){
			dom.classList.add("active-alarm")
			dom.style.color = "red";

		}
	}
}

var class_elemental = {
	dom 	:{}
	,data 	:""
	,screen_cpu 	:""
	,screen_mem 	:""
	,screen_gpu1 	:""
	,screen_gpu2 	:""
	,screen_sdi 	:""
	,screen_psu 	:""
	,screen_slots 	:[1,2,3,4]
	,screen_alarm 	:""
	,setDom:function(dom){
		this.dom = dom
		console.log("class_elemental");
		this.screen_cpu 	= this.dom.querySelector('.cpu .value');
		this.screen_mem 	= this.dom.querySelector('.mem .value');
		this.screen_gpu_pct	= this.dom.querySelector('.gpus.pct');
		this.screen_gpu_temp= this.dom.querySelector('.gpus.temp');
		this.screen_sdi 	= this.dom.querySelector('.sdi .value');
		this.screen_psu 	= this.dom.querySelector('.psu .value');
		this.screen_slot 	= this.dom.getElementsByClassName('slot');
		this.screen_alarm 	= this.dom.querySelector('.alarm .value');
	}
	,setData:function(data){
		if(data.data =='request fail'){
			this.dataSys = null;
			return false;
		}else{
			this.dataSys= JSON.parse(data.data);
			return true;
		}
	}
	,updateData:function(){
		this._resetAlarm();
		if(this.dataSys == null){
			this._showAlarm();
			return false;
		}
		this.screen_cpu.innerHTML 		= this.dataSys.cpu+"%";	
		this.screen_mem.innerHTML 		= this.dataSys.disk_available+"%";	
		this.screen_sdi.innerHTML 		= ""
		this.screen_psu.innerHTML 		= ""
		this.screen_gpu_pct.innerHTML 	= "";
		this.screen_gpu_temp.innerHTML 	= "";
		this._setCpuPctStatus(this.screen_cpu,parseInt(this.dataSys.cpu));
		this._setMemPctStatus(this.screen_mem,parseInt(this.dataSys.disk_available));
		this._setSdiStatus(this.screen_sdi,this.dataSys.sdi_ingest_status);
		this._setPsuPctStatus(this.screen_psu,this.dataSys.psu_status);

		/*for update gpus*/
			/*pct*/
		this._setGpuPctStatus(this.screen_gpu_pct,this.dataSys.gpu)
		this._setGpuTempStatus(this.screen_gpu_temp,this.dataSys.gpu_temperatures)

		/*for update devices card*/
		this._setSlotStatus(this.dataSys.devices, this.screen_slot);
	}
	,_showAlarm:function(){
		this.dom.className += " active-alarm";
	}
	,_resetAlarm:function(){
		this.dom.className = 'server'
	}
	,_setGpuPctStatus:function(dom,data){
		for(var i in data){
			var aGpuPct = parseInt(data[i]['pct']);
			var newDom = document.createElement("div");
			newDom.className = "gpu-pct-"+(parseInt(i)+1);
			newDom.innerHTML = aGpuPct+"%";
			newDom.classList.remove("active-alarm")
			if(aGpuPct<100){
				newDom.style.backgroundColor = "#27B04E";
			}else if(aGpuPct>100){
				newDom.style.backgroundColor = "red";
				newDom.classList.add("active-alarm")
			}
			dom.append(newDom);
		}
	}
	,_setGpuTempStatus:function(dom,data){
		for(var i in data){
			var aGpuTemp = parseInt(data[i]);
			var newDom = document.createElement("div");
			newDom.className = "gpu-temp--"+i;
			newDom.innerHTML = aGpuTemp+"C";
			newDom.classList.remove("active-alarm")
			if(aGpuTemp<60){
				newDom.style.backgroundColor = "#27B04E";
			}else if(aGpuTemp>60){
				newDom.style.backgroundColor = "red";
				newDom.classList.add("active-alarm")
			}
			dom.append(newDom);
		}
	}
	,_setCpuPctStatus(dom,data){
		dom.classList.remove("active-alarm")
		if(data<80){
			dom.style.color = "#27B04E";
		}else if(data>80){
			dom.style.color = "red";
			dom.classList.add("active-alarm")
		}
	}
	,_setMemPctStatus(dom,data){
		if(data<90){
			dom.style.color = "#27B04E";
		}else if(data>90){
			dom.style.color = "red";
		}
	}
	,_setSdiStatus(dom,data){
		var newDom = document.createElement("a");
		newDom.title = data;
		if(data == 'ok'){
			newDom.innerHTML = 'ok';
			newDom.style.color = '#27B04E'
		}else{
			newDom.innerHTML = 'error';
			newDom.style.color = 'red'
		}
		dom.append(newDom)
	}
	,_setPsuPctStatus(dom,data){
		var newDom = document.createElement("a");
		newDom.title = data;
		if(data =='Status:Present,OK'){
			newDom.innerHTML = 'ok';
			newDom.style.color = '#27B04E'
		}else{
			newDom.innerHTML = 'error';
			newDom.style.color = 'red'
		}
		dom.append(newDom)
	}
	,_setSlotStatus : function(data,slots_dom){
		// console.log(data)
		/*Reset slot color to not found*/
		for(var q=0; q<slots_dom.length ; q++){
			slots_dom[q].style.backgroundColor  = "red";
			slots_dom[q].style.color  = "white";
		}

		/*update slot data*/
		for(var j=0; j<slots_dom.length ; j++){
			var aSlot = slots_dom[j]
			for(var i in data){
				var card_data = data[i]
				if((j+1 == card_data.channel) 
					&& (card_data.device_name.search("Quadrant4k")==-1))
				{
					aSlot.title = card_data.device_name+" ("+card_data.description+")"
								+" \r\nVIDEO INFO : "+card_data.video_information;
					/*add audio info to title*/
					if(card_data.audio_information==null){
						aSlot.title+="\r\nAUDIO INFO : null";
					}else if(card_data.audio_information!=null){
						if(card_data.audio_information.channel_pair.length == undefined){
							var pair = card_data.audio_information.channel_pair
							aSlot.title+="\r\nAUDIO INFO : ";
							aSlot.title+="\r\n   id : "+pair.id
										+", format : "+pair.format
										+", dbfs : "+pair.dbfs;
						}else{
							aSlot.title+="\r\nAUDIO INFO : [";
							for(var q in card_data.audio_information.channel_pair){
								var pair = card_data.audio_information.channel_pair[q]
								aSlot.title+="\r\n   id : "+pair.id
											+", format : "+pair.format
											+", dbfs : "+pair.dbfs;
							}aSlot.title+="\r\n]";
						}
					}

					aSlot.style.backgroundColor  = "white";/*Card has INPUT signal*/
					/*Check sdi input*/
					aSlot.classList.remove("active-alarm");
					if(card_data.video_information !="PAL"
						&&card_data.video_information !="1080i50"
						&&card_data.video_information !="NTSC")
					{
						aSlot.title += "\r\nSTATUS: NO SDI INPUT";
						aSlot.style.backgroundColor  = "red";
						// aSlot.style.color  = "white";
						aSlot.classList.add("active-alarm");
						continue;
					}
					/**For DeltacastCaptureCard*/
					if(card_data.description.search("DeltacastCaptureCard")>-1){
						if(card_data.device_status == "AVAILABLE"){
							aSlot.title += "\r\nSTATUS : RUNNING";
							aSlot.style.backgroundColor  = "#27B04E";
							// aSlot.style.color  = "white";
						}else{
							aSlot.title += "\r\nSTATUS : NO STREAM RUNNING";
							aSlot.style.backgroundColor  = "yellow";
							aSlot.style.color  = "black";
						}
					}/*For AJACaptureCard*/
					else if(card_data.description.search('AJACaptureCard')>-1){
						if(card_data.device_status == "RUNNING"){
							aSlot.title += "\r\nSTATUS : RUNNING";
							aSlot.style.backgroundColor  = "#27B04E";
							// aSlot.style.color  = "white";
						}else if(card_data.device_status == "AVAILABLE"
							||card_data.device_status == "STOPPED"){
							aSlot.title += "\r\nSTATUS : NO STREAM RUNNING";
							aSlot.style.backgroundColor  = "yellow";
							aSlot.style.color  = "black";
						}
					}
					
				}
			}
		} 	
	}
}

var class_mediaexcel={
	dom 	:""
	,data 	:""
	,screen_dev_running:""
	,alarm 	:""
	,setDom:function(dom){
		this.dom = dom
		console.log("class_mediaexcel");
		this.screen_dev_running = this.dom.querySelector('.dev_running');
		this.screen_alarm 		= this.dom.querySelector('.alarm');
	}
	,setData:function(data){
		if(data.data =='request fail'){
			this.dataSys = null;
			return false;
		}else{
			this.dataSys= JSON.parse(data.data);
			return true;
		}
	}
	,updateData:function(){
		this._resetAlarm();
		if(this.dataSys == null){
			this._showAlarm();
			return false;
		}
		var dev_val_ctn = this.screen_dev_running.querySelector(".value")
		dev_val_ctn.innerHTML = ""
		dev_val_ctn.append(this._setStatus(this.dataSys.deviceIsRunning.resultString))
	}
	,_showAlarm:function(){
		this.dom.className += " active-alarm";
	}
	,_resetAlarm:function(){
		this.dom.className = 'server'
	}
	,_setStatus:function(data){
		var dom = document.createElement("a");
		dom.title = data;
		if(data == '1'){
			dom.innerHTML = 'ok';
			dom.style.color = '#27B04E'
			dom.style.lineHeight= 2.7;
			dom.style.fontSize=21;
		}else{
			dom.innerHTML = 'no stream running';
			dom.style.color = 'gray'
			dom.style.lineHeight= 1.1;
		    dom.style.wordSpacing= "17px";
		    dom.style.fontSize= "17px";
		}
		return dom
	}
}

var class_main = {
	harmonic:{}
	,elemental:{}
	,mediaexcel:{}
	,dataTimeDom:""
	,getDataTimeDom:""
	,requestTime:""
	,construct:function(){
		var nodeList = class_layout.construct()
		// console.log(nodeList)
		// this.harmonic = nodeList.harmonic;
		// this.elemental = nodeList.elemental;
		// this.mediaexcel = nodeList.mediaecxel;
		this._wrapNodeWithClass(nodeList)
		this.dataTimeDom = document.querySelector(".data-time").childNodes[1];
		this.getDataTimeDom = document.querySelector(".get-time").childNodes[1];

	}
	,recieveData : function(){
		var that = this;
		this._getData();
		setInterval(function(){
			that._getData()
		}, 2000);
	}
	,_getDateNow:function(){
		var dNow 	= new Date();
		return dNow.getFullYear()+"-"+(dNow.getMonth()+1)+"-"+dNow.getDate()+" "
				+dNow.getHours()+":"+dNow.getMinutes()+":"+dNow.getSeconds()
	}
	,_getData	: function(){
		var that 	= this;
		var xhttp = new XMLHttpRequest();
	  	xhttp.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	that.requestTime = that._getDateNow();
		     	var parse 	= JSON.parse(this.responseText);
		     	that._setData(parse);
		    }
	  	};
	  	xhttp.open("GET", "/ott_monitoring/api.php?get", true);
	  	xhttp.send();
	}
	,_setTime : function(time){
		this.dataTimeDom.innerHTML = time
		this.getDataTimeDom.innerHTML= this.requestTime
	}
	,_setData : function(data){ 
		for(var i in data){
			// console.log(data[i])
			var server_type = data[i]['server_type']
			if(server_type == "harmonic_snmp"){
				this.harmonic[data[i]['server_name']].setData(data[i])
				this.harmonic[data[i]['server_name']].updateData()
			}
			else if(server_type == "elemental_http"){
				this.elemental[data[i]['server_name']].setData(data[i])
				this.elemental[data[i]['server_name']].updateData()
			}
			else if(server_type == "mediaexcel_snmp"){
				this.mediaexcel[data[i]['server_name']].setData(data[i])
				this.mediaexcel[data[i]['server_name']].updateData()
			}
		}
		this._setTime(data[0].create_at)
	}
	,_wrapNodeWithClass:function(allNode){/* Init server oject here*/
		/*server factory method*/
		for(var nodeType in allNode){/*nodeType == [harmonic,elemental,mediaexcel]*/
			var nodeOfType = allNode[nodeType]
			for(var i in nodeOfType){ 
				var serverDom = nodeOfType[i]
				if(nodeType == "harmonic"){
					var server = Object.create(class_harmonic)
					server.setDom(serverDom)
					server.name = i
					this.harmonic[i] = server
				}
				else if(nodeType == "elemental"){
					var server = Object.create(class_elemental)
					server.setDom(serverDom)
					server.name = i
					this.elemental[i] = server
				}
				else if(nodeType == "mediaexcel"){
					var server = Object.create(class_mediaexcel)
					server.setDom(serverDom)
					server.name = i
					this.mediaexcel[i] = server
				}
			}
		}
		// console.log(this.harmonic)

	}
}

var class_layout={
	construct:function(){
		var nodeList = {};
		for(var layout_serv_name in LAYOUT){
			var layout_server = LAYOUT[layout_serv_name];
			for(var config_serv_name in SERVER_CONFIG){
				var config_servers = SERVER_CONFIG[config_serv_name]
				if(layout_serv_name == config_serv_name){
					nodeList[layout_serv_name] 
						= this.makeServer(layout_server,config_servers,config_serv_name)
				}
			}
		}
		return nodeList;
	}
	,makeServer:function(layout, serv_list, server_type){
		var blockNodeReturn = {}
		var block = document.body.querySelector(layout.root);
		var container= block.querySelector(".server-container")
		var mockup = container.children[0].cloneNode(true);
		/*remove mockup*/
		while (container.firstChild) {
		  container.removeChild(container.firstChild);
		}
		for(var i in serv_list){
			var server = serv_list[i]
			var newNode 	= mockup.cloneNode(true);
			newNode.querySelector(".name").innerHTML = server.title.toUpperCase()
			if(server_type == "elemental"){
				/* create cards layout*/
				var slots_ctn = newNode.querySelector(".slots");
				for(var j=1 ; j<=server.cardN ; j++){
					var card = document.createElement("div");
					card.className = "slot slot-"+j;
					card.innerHTML = j;
					slots_ctn.append(card);
				}
			}else if(server_type == "harmonic"){
				/* optional */
			}else if(server_type == "mediaexcel"){
				/* optional */
			}
			container.append(newNode);
			blockNodeReturn[server.name] = newNode;
		}
		return blockNodeReturn;
	}
}

class_main.construct()
class_main.recieveData();
