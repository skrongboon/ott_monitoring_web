<?php
Class Database{
	protected $host;
	protected $user;
	protected $pass;
	protected $dbName;

	protected $dbc;
	protected $stmt;

	public function __construct($host,$user,$pass,$db){
		$this->host 	= $host;
		$this->dbName 	= $db;
		$this->user 	= $user;
		$this->pass 	= $pass;

		/*Use PDO*/
		try {
			// $this->dbc 	= new PDO("mysql:host={$this->host};dbname={$this->dbName}",$this->user);
			
			$this->dbc 	= new PDO("mysql:host={$this->host};dbname={$this->dbName}",$this->user,$this->pass);
		}catch(PDOException $e){
			throw new PDOException($e->getMessage());
			die();
		}
	}

	public function prepare($query){
		$this->stmt 	= $this->dbc->prepare($query);
	}

	public function bindParam($key,$value){
		$this->stmt->bindParam($key, $value);
	}

	public function execute(){
		$this->stmt->execute();
	}

	public function fetch($fetchStyle = PDO::FETCH_OBJ){
		return $this->stmt->fetch($fetchStyle);
	}

	public function fetchAll($fetchStyle = PDO::FETCH_OBJ){
		return $this->stmt->fetchAll($fetchStyle);
	}
}